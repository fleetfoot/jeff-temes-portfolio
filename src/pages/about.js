import React from "react";
import styled from "styled-components";

import Layout from "../components/layout";
import SEO from "../components/seo";
import Image from "../components/image";
import PageTitle from "../components/page-title";

const ImageWrapper = styled.div`
  max-width: 180px;
  position: relative;
  z-index: 1;
  margin-bottom: 1.5rem;

  &:after {
    height: initial;
    top: 0.3em;
    left: 0.3em;
    right: -0.3em;
    bottom: -0.3em;
    background: linear-gradient(
      65deg,
      var(--tertiary-color),
      var(--secondary-color)
    );
    content: "";
    position: absolute;
    z-index: -1;
  }

  &:first-child {
    z-index: 1;
    opacity: 1;
  }
`;

const AboutMain = styled.div`
  @media screen and (min-width: 600px) {
    display: grid;
    grid-template-columns: 180px 1fr;
    grid-column-gap: 1.5em;
  }
`;

const Styledh3 = styled.h3`
  border-bottom: 1px solid var(--tertiary-color);
  display: inline-block;
`;

const Styleda = styled.a`
  &:hover,
  &:focus {
    box-shadow: inset 0 -0.2em 0 var(--secondary-color);
  }
`;

const Blurb = styled.div``;

const BlurbP = styled.p`
  margin-bottom: 0.725em;
`;

const About = () => (
  <Layout>
    <SEO title="About" />
    <PageTitle>About Me</PageTitle>
    <div className="content">
      <AboutMain>
        <ImageWrapper>
          <Image />
        </ImageWrapper>
        <Blurb>
          <BlurbP>
            Hello there. I’m a web developer with 3 years of freelancing
            experience. In that time I've had to learn new stacks and maintain
            old ones. It's a never ending project which I love.
          </BlurbP>

          <BlurbP>
            l enjoy tinkering with Linux machines and run a server where I host
            my projects, dive deep into security, and play with new tools. I
            have a keen eye for design and I'm not happy until every little
            detail looks right.
          </BlurbP>

          <BlurbP>
            When I’m not on my computer I’m traveling, strumming my guitar,
            baking sourdough bread, making kimchi, or playing hockey.
          </BlurbP>
        </Blurb>
      </AboutMain>
      <br />
      <Styledh3>Current tools</Styledh3>
      <ul>
        <li>
          <strong>Laptop: </strong>
          <Styleda
            href="https://www.lenovo.com/ca/en/laptops/thinkpad/thinkpad-t-series/ThinkPad-T470s/p/22TP2TT470S"
            target="_blank"
            rel="noopener noreferrer"
          >
            ThinkPad T470s
          </Styleda>
        </li>
        <li>
          <strong>OS: </strong>
          <Styleda
            href="https://getfedora.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Fedora 37
          </Styleda>
        </li>
        <li>
          <strong>Server: </strong>
          <br />
          <ul>
            <li>
              VPS:{" "}
              <Styleda
                href="https://www.linode.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Linode
              </Styleda>
            </li>
            <li>
              OS:{" "}
              <Styleda
                href="https://www.debian.org/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Debian 10
              </Styleda>
            </li>
            <li>
              Reverse Proxy:{" "}
              <Styleda
                href="https://traefik.io/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Traefik
              </Styleda>
            </li>
            <li>
              Orchestrator:{" "}
              <Styleda
                href="https://www.docker.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Docker
              </Styleda>
            </li>
          </ul>
        </li>
        <li>
          <strong>Frameworks/Libraries: </strong>
          <Styleda
            href="https://www.gatsbyjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Gatsby
          </Styleda>
          ,&nbsp;
          <Styleda
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React.js
          </Styleda>
        </li>
        <li>
          <strong>CMS: </strong>
          <Styleda
            href="https://www.wordpress.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            WordPress
          </Styleda>
        </li>
        <li>
          <strong>Editor: </strong>
          <Styleda
            href="https://github.com/hlissner/doom-emacs"
            target="_blank"
            rel="noopener noreferrer"
          >
            Doom Emacs
          </Styleda>
        </li>
        <li>
          <strong>Theme: </strong>
          <Styleda
            href="https://protesilaos.com/emacs/modus-themes"
            target="_blank"
            rel="noopener noreferrer"
          >
            Modus
          </Styleda>
        </li>
      </ul>
    </div>
  </Layout>
);

export default About;
