import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";
import PageTitle from "../components/page-title";
import styled from "styled-components";

import gitlab from "../images/gitlab.svg";
// import phone from "../images/phone.svg";
import mail from "../images/mail.svg";
import linkedin from "../images/linkedin.svg";

const ContactP = styled.p`
  margin: 0;
`;

const EmailP = styled.p`
  margin-bottom: 10px;
`;

const SmallIcon = styled.img`
  width: 12px;
  margin: 0;
  margin-right: 15px;

  @media screen and (min-width: 700px) {
    width: 15px;
  }
`;

const IconContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 3rem;
`;

const IconLink = styled.a`
  box-shadow: none;
  text-align: center;
  position: relative;
  margin-right: 3em;
`;

const IconImage = styled.img`
  width: 50px;
  margin-bottom: 0.2em;

  @media screen and (min-width: 700px) {
    width: 50px;
  }
`;

const Contact = () => (
  <Layout>
    <SEO title="Contact" />
    <PageTitle>Contact</PageTitle>
    <div className="content">
      <ContactP>
        Interested in working with me? I would love to hear from you. Send me an
        email and I’ll get back to you.
        <br />
        <br />
      </ContactP>
      <EmailP>
        <SmallIcon src={mail} alt="Email" />
        <a href="mailto:hello@jefftemes.com?Subject=Hello%20Jeff">
          hello@jefftemes.com
        </a>
      </EmailP>
      <IconContainer>
        <IconLink
          href="mailto:hello@jefftemes.com?Subject=Hello%20Jeff"
          target="_blank"
          rel="noopener noreferrer"
        >
          <IconImage src={mail} alt="Email Logo" />
          <p>Email</p>
        </IconLink>
        <IconLink
          href="https://gitlab.com/fleetfoot"
          target="_blank"
          rel="noopener noreferrer"
        >
          <IconImage src={gitlab} alt="Gitlab Logo" />
          <p>Gitlab</p>
        </IconLink>
        <IconLink
          href="https://www.linkedin.com/in/jeff-temes"
          target="_blank"
          rel="noopener noreferrer"
        >
          <IconImage src={linkedin} alt="Linkedin Logo" />
          <p>Linkedin</p>
        </IconLink>
      </IconContainer>
    </div>
  </Layout>
);

export default Contact;
