import React from "react";
import styled from "styled-components";

import { graphql } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";
import PageTitle from "../components/page-title";
import Project from "../components/project";

console.log(
  "     ____.       _____  _____         \r\n    |    | _____/ ____\\/ ____\\        \r\n    |    |/ __ \\   __\\\\   __\\         \r\n/\\__|    \\  ___/|  |   |  |           \r\n\\________|\\___  >__|   |__|           \r\n              \\/                      \r\n___________                           \r\n\\__    ___/___   _____   ____   ______\r\n  |    |_/ __ \\ /     \\_/ __ \\ /  ___/\r\n  |    |\\  ___/|  Y Y  \\  ___/ \\___ \\ \r\n  |____| \\___  >__|_|  /\\___  >____  >\r\n             \\/      \\/     \\/     \\/ \r\n    .___      __                      \r\n  __| _/_____/  |_                    \r\n / __ |/  _ \\   __\\                   \r\n/ /_/ (  <_> )  |                     \r\n\\____ |\\____/|__|                     \r\n     \\/                               \r\n                                      \r\n  ____  ____   _____                  \r\n_/ ___\\/  _ \\ /     \\                 \r\n\\  \\__(  <_> )  Y Y  \\                \r\n \\___  >____/|__|_|  /                \r\n     \\/            \\/   "
);

const Portfolio = styled.ul`
  list-style: none;
  margin: 0;
`;

const IndexPage = ({ data }) => (
  <Layout>
    <SEO
      title="Portfolio"
      keywords={[`jeff`, `temes`, `web`, `developer`, `portfolio`]}
    />
    <PageTitle>Portfolio</PageTitle>
    <div className="content">
      <Portfolio>
        <Project
          title="Storage Warrior"
          image={data.storageWarrior.childImageSharp.fluid}
          techs={["WordPress", "JavaScript", "Sass"]}
          description="I had a lot of fun building this website. It starts with WordPress Underscores starter theme, Gulp 4 as a task runner, SASS for writing clean BEM styles and Webpack to compile the JavaScript. The WordPress backend uses ACF for page templates and Gutenberg for posts and non-templated pages. I used Cloudflare for caching and SSL, and server-side caching for additional speed."
          siteLink="https://storagewarrior.ca"
        />

        <Project
          title="Beanworks"
          image={data.beanworks.childImageSharp.fluid}
          techs={["WordPress", "JavaScript", "Sass"]}
          description="Beanworks is a custom theme built in WordPress using Advanced Custom Fields, custom post types,
          custom widgets and taxonomies to create an easy-to-edit backend. I used Gulp to compile Sass and JavaScript ES6. It's a
          fully responsive theme using CSS Grid and Flexbox."
          siteLink="https://www.beanworks.com"
        />

        <Project
          title="Verus Art"
          image={data.verusArt.childImageSharp.fluid}
          techs={["Shopify", "Liquid", "Ajax"]}
          description="This an ecommerce site built with Shopify.
          The theme is fully responsive and customized for Verus Art so they can easily add new artists and pages without needing to dig into the code.
          Shopify does not make it easy to change the layout so I had to find a way to use metafields to allow the client to easily update the
          content while working within the design."
          siteLink="https://www.verusart.com"
        />

        <Project
          title="Talk Social To Me"
          image={data.tstm.childImageSharp.fluid}
          techs={["WordPress", "PHP", "Sass"]}
          description="For the frontend I used modern CSS techniques (CSS Grid, Flexbox) to create responsive pages, widgets and templates. 
          
          For the backend I wanted to create a simple, clean, and intuitive layout with minimal plugins. I used custom post-types, taxonomies and widgets and tailored them with Advanced Custom Fields to make them easy for the client to edit pages and create new ones."
          siteLink="https://www.talksocialtome.com"
        />

        <Project
          title="SVG Fish"
          image={data.fishImage.childImageSharp.fluid}
          techs={["Blender", "SVG", "Freestyle"]}
          description="I've always wanted to learn 3D animation, so I decided to turn a doodle of mine into an SVG animation. The fish are originally 3D objects in Blender, I then used Freestyle SVG Exporter to create animated SVG's. It required some tweaking of the SVG code and filling in colors in Inkscape but finally I got something I was happy with."
        />

        <Project
          title="Hi Clouds"
          image={data.hiClouds.childImageSharp.fluid}
          techs={["Vue.js", "JavaScript (ES6)"]}
          description="This project was created to pull weather data from Yr.no specifically for the use of quickly getting important cloud information for finding the northern lights. But of course it can be used for any use case when low, medium and high clouds are needed.
I built this using Vue.js, vanilla JavaScript (ES6) for the API calls and Sass. The hardest part of this project was navigating an API in Norwegian."
          /* siteLink="https://auro.jefftemes.xyz" */
          codeLink="https://gitlab.com/fleetfoot/auro-weather"
        />

        <Project
          title="Mars Colony"
          image={data.mars.childImageSharp.fluid}
          techs={["Angular", "ES6", "Sass"]}
          description="An Angular App for signing up as a Mars colonist. Posting your name and profession to a central database (no longer live which is why this app is not live) and display a blog pulled from a WordPress REST API."
          codeLink="https://gitlab.com/fleetfoot/Mars-Colony"
        />

        <Project
          title="Hockey Pong"
          image={data.pong.childImageSharp.fluid}
          techs={["JavaScript ES6", "SVG"]}
          description="This project was to better understand Object-Oriented Programming with JavaScript, working with some of the new features of ES6 as well as getting my hand's dirty creating SVG's with HTML. I learned basic game design concepts such as the game loop, hit-boxes and assigning controls to move rendered SVG's with the keyboard."
          // siteLink="https://fleetfoot.gitlab.io/Hockey-Pong"
          codeLink="https://gitlab.com/fleetfoot/Hockey-Pong"
        />

        <Project
          title="Instanews App"
          image={data.instanews.childImageSharp.fluid}
          techs={["JavaScript", "JQuery", "Ajax"]}
          description="The goal of the project was to learn how to use Ajax to retrieve content from an API as well as getting used to working with a preprocessor (Gulp), Sass and various other Node Modules. Mobile view was first implemented using Flexbox. This is now the fallback view for browsers which do not support CSS Grid. If CSS Grid is supported then it is used in 3 different views: Desktop ( > 1000px), Tablet, and Mobile ( < 600px)."
          siteLink="https://fleetfoot.gitlab.io/Instanews-App"
          codeLink="https://gitlab.com/fleetfoot/Instanews-App"
        />

        <Project
          title="Aloha Apparel Co"
          image={data.aloha.childImageSharp.fluid}
          techs={["HTML", "JQuery", "CSS"]}
          description="This was one of the first sites I ever did in web development. It's the homepage of a fake store. The goal was to get comfortable with responsive development in 3 different sizes- mobile, tablet and desktop and getting comfortable with HTML semantics and modern CSS practices."
          siteLink="https://fleetfoot.gitlab.io/Aloha-Apparel"
          codeLink="https://gitlab.com/fleetfoot/Aloha-Apparel"
        />
      </Portfolio>
    </div>
  </Layout>
);

export default IndexPage;

export const query = graphql`
  query {
    storageWarrior: file(relativePath: { eq: "storagewarrior.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    beanworks: file(relativePath: { eq: "beanworks.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    verusArt: file(relativePath: { eq: "verusart.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    hiClouds: file(relativePath: { eq: "hi-clouds.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    fishImage: file(relativePath: { eq: "fish-image.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    tstm: file(relativePath: { eq: "tstm.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    mars: file(relativePath: { eq: "mars.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    pong: file(relativePath: { eq: "pong.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    instanews: file(relativePath: { eq: "instanews.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    aloha: file(relativePath: { eq: "aloha.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;
