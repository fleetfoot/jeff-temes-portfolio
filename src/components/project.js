import React from "react";
import styled from "styled-components";
import Img from "gatsby-image";

const Container = styled.li`
  margin-bottom: 3rem;

  @media screen and (min-width: 700px) {
    display: grid;
    grid-column-gap: 2rem;
    grid-template-columns: [image] 300px [description] 1fr;
    grid-template-rows: [header] 50px [description] min-content;
  }
`;

const Title = styled.h2`
  font-size: 18x;
  grid-column: span 2;
`;

const ImageWrapper = styled.div`
  align-self: start;
  max-width: 350px;
  position: relative;
  z-index: 1;
  opacity: 1;
  margin-bottom: 1.5rem;
  display: block;

  @media screen and (min-width: 700px) {
    grid-column: image;
  }

  &:after {
    height: initial;
    top: 0.3em;
    left: 0.3em;
    right: -0.3em;
    bottom: -0.3em;
    background: linear-gradient(
      65deg,
      var(--tertiary-color),
      var(--secondary-color)
    );
    content: "";
    position: absolute;
    z-index: -1;
  }
`;

const Image = styled(Img)`
  border: 5px solid var(--black);
`;

const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Techs = styled.ul`
  list-style: none;
  display: flex;
  flex-wrap: nowrap;
  margin: 0;
`;

const Tech = styled.li`
  padding: 0.2rem 0.5rem;
  font-size: 15px;
  margin-right: 0.2rem;
  border-radius: 10px;
  height: 33px;
  font-weight: bold;
  // color: var(--white);colorcolor

  background-color: rgba(var(--secondary-color-rgb), 0.1);background-color
`;

const Description = styled.p`
  flex: 1;
  font-size: 17px;
  margin-bottom: 0.5rem;
`;

const Buttons = styled.div`
  padding-bottom: 1.4rem;
`;

const SiteLink = styled.a`
  margin-right: 1rem;
  font-weight: 700;
`;

const CodeLink = styled.a`
  font-weight: 700;
`;

const Project = props => (
  <Container>
    <Title>{props.title}</Title>
    <ImageWrapper>
      <Image fluid={props.image} />
    </ImageWrapper>
    <TextWrapper>
      <Techs>
        {props.techs.map(tech => (
          <Tech key={tech}>{tech}</Tech>
        ))}
      </Techs>
      <Description>{props.description}</Description>
      <Buttons>
        {props.siteLink ? (
          <SiteLink
            href={props.siteLink}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit Site
          </SiteLink>
        ) : null}
        {props.codeLink ? (
          <CodeLink
            href={props.codeLink}
            target="_blank"
            rel="noopener noreferrer"
          >
            Source Code
          </CodeLink>
        ) : null}
      </Buttons>
    </TextWrapper>
  </Container>
);

export default Project;
