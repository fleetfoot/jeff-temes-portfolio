import React from "react";
import styled from "styled-components";

const Title = styled.h1`
  padding: 0.1rem 0.7rem;
  margin-bottom: 1rem;
  text-align: center;
`;

const PageTitle = (props) => <Title>{props.children}</Title>;

export default PageTitle;
