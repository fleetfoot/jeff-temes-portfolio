import PropTypes from "prop-types";
import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";

const Container = styled.header`
  background: transparent;
  margin-bottom: 3.45rem;
  padding-top: 1rem;
`;

const Div = styled.div`
  margin: 0 auto;
  max-width: 960;
  display: grid;
  grid-template-columns: [title] 4fr [nav] 1fr;
`;

const TitleLink = styled(Link)`
  box-shadow: none;
  display: inline-block;

  &:hover,
  &:focus {
    box-shadow: none;
  }
`;

const Title = styled.h1`
  margin: 0 0.2rem 0 0;
  color: var(--white);
  background-color: var(--black);
  // text-transform: uppercase;
  padding: 0.1rem 0.7rem 0.1rem;
  transition: color 0.4s ease-in-out;

  &:hover {
    color: #b99e7e;
  }
`;

const SubTitleWrapper = styled.div`
  position: relative;
  grid-column: title;
  /* margin-top: 0.2rem; */
`;

const SubTitle = styled.h2`
  color: var(--black);
  position: relative;
  padding: 0.2rem 0.7rem;
  margin-top: 0.2rem;
  text-transform: lowercase;
`;

const Nav = styled.nav`
  grid-row: 1/3;
  grid-column: nav;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const StyledLink = styled(Link)`
  margin-bottom: 0.5rem;
  font-size: 18px;
  font-weight: 700;
  padding-right: 1rem;
`;

const Header = ({ siteTitle }) => (
  <Container>
    <Div>
      <TitleLink to="/">
        <Title>{siteTitle}</Title>
      </TitleLink>
      <SubTitleWrapper>
        <SubTitle>Web Developer</SubTitle>
      </SubTitleWrapper>
      <Nav>
        <StyledLink to="/" activeClassName="active">
          Portfolio
        </StyledLink>
        <StyledLink to="/about" activeClassName="active">
          About
        </StyledLink>
        <StyledLink to="/contact" activeClassName="active">
          Contact
        </StyledLink>
      </Nav>
    </Div>
  </Container>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
