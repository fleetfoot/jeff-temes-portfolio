import React from "react";
import PropTypes from "prop-types";
import { StaticQuery, graphql } from "gatsby";

import Header from "./header";
import styled from "styled-components";
import "./layout.css";

const Container = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
`;

const Main = styled.div`
  flex: 1;
`;

const Footer = styled.footer`
  text-align: center;
`;

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Container>
          <Header siteTitle={data.site.siteMetadata.title} />
          <Main
            style={{
              maxWidth: 960,
              // padding: `0px 1.0875rem 1.45rem`,
              paddingTop: 0
            }}
          >
            <main>{children}</main>
          </Main>
          <Footer>
            © {new Date().getFullYear()}, Built by me using Gatsby :)
            {` `}
            <a
              href="https://gitlab.com/fleetfoot/jeff-temes-portfolio"
              target="_blank"
              rel="noopener noreferrer"
            >
              source code here
            </a>
          </Footer>
        </Container>
      </>
    )}
  />
);

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
