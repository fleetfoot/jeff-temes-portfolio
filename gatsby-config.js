module.exports = {
  siteMetadata: {
    title: `Jeff Temes`,
    description: `Jeff Temes Web Developer`,
    author: `@jefftemes`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-catch-links`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/jefftemesfav.png` // This path is relative to the root of the site.
      }
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Lora\:700`,
          `Roboto\:400,700`
        ]
      }
    },
    // To learn more, visit: https://gatsby.app/offline // this (optional) plugin enables Progressive Web App + Offline functionality
    // 'gatsby-plugin-offline',
    `gatsby-plugin-styled-components`
  ]
};
